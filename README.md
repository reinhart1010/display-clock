# Clock

Part of "Display Series"

View Demo: [GitHub](https://reinhart1010.github.io/display-clock/) | [GitLab](https://reinhart1010.gitlab.io/display-clock/)

## Functions

- [ ] Clock color customization
- [x] Dynamic navbar (here it's called `cpanel`)
- [ ] Location-based weather (Geolocation)
- [x] Save settings as cookies
- [x] Simple clock layout
- [x] Time format (12h or 24h) support
- [x] Weather (by [OpenWeatherMap](https://www.openweathermap.org))

## Permissions

+ Cookies
